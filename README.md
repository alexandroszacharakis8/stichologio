# Στιχολόγιο και συλλογή εκτελέσεων

Στιχολόγιο με συλλογή από διάφορα τραγούδια, κυρίως (αλλά όχι μονο) ρεμπέτικα. Στα διάφορα τραγούδια περιλαμβάνονται εκτελέσεις που είτε είναι καθαρές και άρα καλές για εξάσκηση, είτε απλά είναι όμορφες.

Οι τίτλοι των τραγουδιών περιλαμβάνουν τον δρόμο (όχι πάντα σωστά...).

Τέλος, καθώς οι παλιές κυρίως εκτελέσεις είναι κάποιες φορές "ακούρδηστες" για τα σημερινά δεδομένα, περιέχεται δίπλα στις εκτελέσεις μία τονικότητα και το πόσο πρέπει να μεταβάλουμε το τραγούδι για να την πετύχουμε.

Για μεταβολές χρησιμοποιείται το plugin [Music Speed Changer](https://musicspeedchanger.com/) για chromium based browsers. Ο πρώτος αριθμός αντιστοιχεί στο "pitch" (και παραλείπεται όταν είναι 0) και ο δεύτερος στο "Tune". Οι μεταβολές ούτε είναι εντελώς σωστές ούτε χρησιμοποιούν πάντα την κοντινότερη τονική. Τέλος, υπάρχει αρκετή αλλοίωση οπότε το plugin συνίσταται μόνο για γρήγορη λύση. Εναλλακτικά υπάρχει το [Audacity](https://www.audacityteam.org/).

Για κατέβασμα όλων των τραγουδιών από το youtube μέσω linux, έχοντας εγκατεστημένο το [youtube-dl](https://youtube-dl.org/) τρέχουμε από terminal

```
grep -roh 'youtube.com[^}]\+' Τραγούδια/ >> songlist
youtube-dl -a songlist
```

## Building

Run `nix build`. The pdf will be produced in `result/stichologio.pdf`.

## Developing

Set up a local developement environment with `latexmk` and all LaTeX
dependencies installed. If you use nix you can simply run `nix develop` to be
dropped into a shell with all dependencies available.

To build run:

```
latexmk -f- -xelatex -outdir=output -interaction=nonstopmode -synctex=1 main.tex
```
Use the option `-outdir=SOMEDIR` for a clean and tidy compilation.

